import nl.utwente.di.bookQuote.Quoter;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class TestQuoter {
    @Test
    public void TestQuoter() throws Exception {
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("12345");
        Assertions.assertEquals(5.5, price, 0.0, "12345");
    }
}